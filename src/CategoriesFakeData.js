const CategoriesFakeData = [
  {
    title: "Příjmy",
    subCategoryList: [{
      subcategoryName: "Vehiklo",
      subcategoryItems: ['Odběratel 1', 'Odběratel 2', 'Odběratel 3',]
    },
    {
      subcategoryName: "Starky's Club",
      subcategoryItems: ['Odběratel 1', 'Odběratel 2', 'Odběratel 3',]
    },],
  },
  {
    title: "Výdaje",
    subCategoryList: [{
      subcategoryName: "Marketing",
      subcategoryItems: ['Odběratel 1', 'Odběratel 2', 'Odběratel 3',]
    },
    {
      subcategoryName: "Výplaty",
      subcategoryItems: ['Odběratel 1', 'Odběratel 2', 'Odběratel 3',]
    },
    {
      subcategoryName: "Nezařazené",
      subcategoryItems: ['Odběratel 1', 'Odběratel 2', 'Odběratel 3',]
    },],
  },
  {
    title: "Uzavřené",
    subCategoryList: [{
      subcategoryName: "Staré kanceláře",
      subcategoryItems: ['Odběratel 1', 'Odběratel 2', 'Odběratel 3',]
    },
    {
      subcategoryName: "Externí spolupráce",
      subcategoryItems: ['Odběratel 1', 'Odběratel 2', 'Odběratel 3',]
    },],
  },
];

export default CategoriesFakeData;