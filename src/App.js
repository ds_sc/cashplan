/* eslint-disable react/display-name */
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Dashboard from './Pages/Dashboard';
import Cashflow from './Pages/Cashflow';
import Transactions from './Pages/Transactions';
import ManageCategories from './Pages/ManageCategories';
import Settings from './Pages/Settings';
import Help from './Pages/Help';
import Topbar from './Components/Topbar';
import SidebarUserInfo from './Components/SidebarUserInfo';
import SidebarLinks from './Components/SidebarLinks';

const routes = [
  {
    path: "/",
    exact: true,
    main: () => <Dashboard />
  },
  {
    path: "/cashflow/",
    main: () => <Cashflow />
  },
  {
    path: "/transakce/",
    main: () => <Transactions />
  },
  {
    path: "/sprava-kategorii/",
    main: () => <ManageCategories />
  },
  {
    path: "/nastaveni/",
    main: () => <Settings />
  },
  {
    path: "/pomoc/",
    main: () => <Help />
  }
];

class App extends Component {

  render() {
    return (
      <div className="app">
        <Topbar />
        <Router>
          <div style={{ display: "flex" }}>
            <div className="sidebar">
              <SidebarUserInfo />
              <SidebarLinks />
            </div>

            <div style={{ flex: 1, padding: "10px" }}>
              {routes.map((route, index) => (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={route.main}
                />
              ))}
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
