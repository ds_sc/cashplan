import React from 'react';
import { Button, ButtonGroup, Container } from 'reactstrap';
import CashflowGraph from '../Components/CashflowGraph';

export default function Cashflow() {
  return (
    <Container className="cashflow screen">
      <div className="top-filter">
        <h2>Cashflow</h2>
        <ButtonGroup className="filter-buttons">
          <Button active>Vývoj</Button>
          <Button>Přehled</Button>
          <Button>Transakce</Button>
          <Button>Faktury</Button>
        </ButtonGroup>
      </div>
      <div className="graph">
        <CashflowGraph />
      </div>
    </Container>
  );
}
