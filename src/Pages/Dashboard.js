import React from 'react';
import { Button, ButtonGroup, Container } from 'reactstrap';


export default function Dashboard() {
  return (
    <Container className="cashflow screen">
      <div className="top-filter">
        <h2>Dashboard</h2>
        <ButtonGroup className="filter-buttons">
          <Button active>Vývoj</Button>
          <Button>Přehled</Button>
          <Button>Transakce</Button>
          <Button>Faktury</Button>
        </ButtonGroup>
      </div>
    </Container>
  );
}
