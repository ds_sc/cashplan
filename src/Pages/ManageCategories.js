import React from 'react';
import { Button, ButtonGroup, Container } from 'reactstrap';
import CategoryToggle from '../Components/CategoryToggle';


export default function ManageCategories() {
  return (
    <Container className="categories screen">
      <div className="top-filter">
        <h2>Správa kategorií</h2>
        <ButtonGroup className="filter-buttons">
          <Button>Vytvořit novou subkategorii</Button>
          <Button active>Vytvořit novou kategorii</Button>
        </ButtonGroup>
      </div>
      <div className="categories-management">
        <div className="incomes category">
          <CategoryToggle title="Příjmy" color="#32D172" />
        </div>
        <div className="expenses category">
          <CategoryToggle title="Výdaje" color="#FF7474" />
        </div>
        <div className="concluded category">
          <CategoryToggle title="Uzavřené" />
        </div>
      </div>
    </Container>
  );
}
