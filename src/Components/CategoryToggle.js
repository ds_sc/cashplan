import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Collapse, ListGroup, ListGroupItem } from 'reactstrap';
import ArrowTriangle from '../Icons/ArrowTriangle';


class CategoryToggle extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.toggleSub = this.toggleSub.bind(this);
    this.state = { collapse: false, collapseSub: false };
  }

  toggle() {
    this.setState(state => ({ collapse: !state.collapse }));
  }

  toggleSub() {
    this.setState(state => ({ collapseSub: !state.collapseSub }));
  }

  render() {
    const { color, title } = this.props;
    return (
      <div>
        <div className="category-title">
          <h4 onClick={this.toggle} style={{ margin: '0', fontWeight: 'bold' }}>{title}</h4>
          <div className="category-color" style={
            {
              backgroundColor: color,
              width: '16px',
              height: '16px',
              borderRadius: '8px',
              margin: '0 10px 0 20px'
            }
          } />
          <ArrowTriangle fill="#2E2E2E" />
        </div>
        <Collapse isOpen={this.state.collapse}>
          <ListGroup>
            <ListGroupItem onClick={this.toggleSub}>
              Vehiklo
              <Collapse isOpen={this.state.collapseSub}>
                <ListGroup>
                  <ListGroupItem>Cras justo odio</ListGroupItem>
                  <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
                  <ListGroupItem>Morbi leo risus</ListGroupItem>
                  <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
                  <ListGroupItem>Vestibulum at eros</ListGroupItem>
                </ListGroup>
              </Collapse>
            </ListGroupItem>
          </ListGroup>
        </Collapse>
      </div>
    );
  }
}

CategoryToggle.propTypes = {
  color: PropTypes.string,
  title: PropTypes.string.isRequired,
}

export default CategoryToggle;