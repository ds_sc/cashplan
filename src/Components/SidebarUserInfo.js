import React from 'react';
import userpic from '../Icons/userpic.png'

export default function SidebarUserInfo() {
  return (
    <div className="user-info">
      <div className="user-pic">
        <img src={userpic} alt="user" />
      </div>
      <div className="user-name">
        <span className="name">M. Švach</span>
        <span className="role">Vlastník</span>
      </div>
    </div>
  )
}