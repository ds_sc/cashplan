import React from 'react';
import { NavLink } from 'react-router-dom';
import DashboardIcon from '../Icons/DashboardIcon';
import CashflowIcon from '../Icons/CashflowIcon';
import TransactionsIcon from '../Icons/TransactionsIcon';
import CategoriesIcon from '../Icons/CategoriesIcon';
import SettingsIcon from '../Icons/SettingsIcon';
import HelpIcon from '../Icons/HelpIcon';

export default function SidebarLinks() {
  return (
    <ul style={{ listStyleType: "none", padding: '10px 0' }}>
      <li className="sidebar-link">
        <NavLink exact to="/" activeClassName="active">
          <DashboardIcon fill="none" />
          Dashboard
        </NavLink>
      </li>
      <li className="sidebar-link">
        <NavLink to="/cashflow" activeClassName="active">
          <CashflowIcon fill="none" />
          Cashflow
        </NavLink>
      </li>
      <li className="sidebar-link">
        <NavLink to="/transakce" activeClassName="active">
          <TransactionsIcon fill="none" />
          Transakce
        </NavLink>
      </li>
      <li className="sidebar-link">
        <NavLink to="/sprava-kategorii" activeClassName="active">
          <CategoriesIcon fill="none" />
          Správa kategorií
          </NavLink>
      </li>
      <li className="sidebar-link" style={{ paddingTop: '40px' }}>
        <NavLink to="/nastaveni" activeClassName="active">
          <SettingsIcon fill="none" />
          Nastavení
        </NavLink>
      </li>
      <li className="sidebar-link">
        <NavLink to="/pomoc" activeClassName="active">
          <HelpIcon fill="none" />
          Pomoc
        </NavLink>
      </li>
    </ul>
  )
}
