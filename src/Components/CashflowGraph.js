import React from 'react';
import cashflowGraph from '../Icons/graf.png';

export default function CashflowGraph() {
  return (
    <div className="cashflow-graph">
      <img src={cashflowGraph} alt="cashflow graph" />
    </div>
  )
}