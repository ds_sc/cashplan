import React from 'react';
import bell from '../Icons/bell.png';

export default function Topbar() {
	return (
		<div className="topbar">
			<nav className="navbar custom navbar-expand-lg">
				<a className="navbar-brand custom" href="/">CashPlan</a>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>

				<div className="collapse navbar-collapse right" id="navbarSupportedContent">
					<ul className="navbar-nav">
						<li className="nav-item">
							<span className="nav-link">Po 13.08.2018</span>
						</li>
						<li className="nav-item">
							<span className="nav-link">Na účtu <span>2 167 898 Kč</span></span>
						</li>
						<li className="nav-item">
							<span className="nav-link">Obrat 2018 <span>3 125 858 Kč</span></span>
						</li>
						<li className="nav-item dropdown custom col">
							<span className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Vehiklo Ventil s.r.o.
              </span>
							<div className="dropdown-menu" aria-labelledby="navbarDropdown">
								<span className="dropdown-item">Action</span>
								<span className="dropdown-item">Another action</span>
								<div className="dropdown-divider"></div>
								<span className="dropdown-item">Something else here</span>
							</div>
						</li>
						<li className="nav-item bell col">
							<img src={bell} alt="" />
						</li>
					</ul>
				</div>
			</nav>
		</div>
	)
}