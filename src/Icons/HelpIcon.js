import React from 'react';
import PropTypes from 'prop-types';

HelpIcon.propTypes = {
  fill: PropTypes.string.isRequired,
}

export default function HelpIcon(props) {
  return (
    <svg width="21" height="20" viewBox="0 0 21 20" fill={props.fill} xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
        <path d="M10.6 19.1666C15.6626 19.1666 19.7666 15.0625 19.7666 9.99992C19.7666 4.93731 15.6626 0.833252 10.6 0.833252C5.53734 0.833252 1.43329 4.93731 1.43329 9.99992C1.43329 15.0625 5.53734 19.1666 10.6 19.1666Z" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M10.5658 15.8332C11.0261 15.8332 11.3991 15.4601 11.3991 14.9998C11.3991 14.5396 11.0261 14.1665 10.5658 14.1665C10.1056 14.1665 9.73248 14.5396 9.73248 14.9998C9.73248 15.4601 10.1056 15.8332 10.5658 15.8332Z" fill="#2E2E2E" />
        <path d="M8.77661 5.46647C10.4266 4.73731 12.5616 4.82397 13.2908 6.00397C14.0199 7.18397 13.5166 8.55647 12.2666 9.61564C11.0166 10.6748 10.5658 11.2498 10.5658 12.0831" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width="20" height="20" fill="white" transform="translate(0.599976)" />
        </clipPath>
      </defs>
    </svg>
  )
}


