import React from 'react';
import PropTypes from 'prop-types';

ArrowTriangle.propTypes = {
  fill: PropTypes.string.isRequired,
}

export default function ArrowTriangle(props) {
  return (
    <svg width="10" height="9" viewBox="0 0 10 9" fill={props.fill} xmlns="http://www.w3.org/2000/svg">
      <path d="M5.94602 8.30005C5.56112 8.96672 4.59887 8.96672 4.21397 8.30005L0.819154 2.42005C0.434254 1.75338 0.915381 0.920049 1.68518 0.920049L8.47482 0.920048C9.24462 0.920048 9.72574 1.75338 9.34084 2.42005L5.94602 8.30005Z" fill="#2E2E2E" />
    </svg>
  )
}


