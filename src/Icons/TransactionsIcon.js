import React from 'react';
import PropTypes from 'prop-types';

TransactionsIcon.propTypes = {
  fill: PropTypes.string.isRequired,
}

export default function TransactionsIcon(props) {
  return (
    <svg width="21" height="20" viewBox="0 0 21 20" fill={props.fill} xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
        <path d="M9.7666 4.58325H16.4333" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M9.7666 7.9165H16.4333" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M9.7666 11.25H16.4333" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M1.43329 16.6667V19.1667H11.4333V17.0137C11.4333 15.8633 12.3662 15 13.5166 15H3.09996C2.17954 15 1.43329 15.7462 1.43329 16.6667Z" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" />
        <path d="M13.5166 14.9999C14.5945 14.9999 15.6 15.7695 15.6 17.0833C15.6 18.2337 16.5329 19.1666 17.6833 19.1666C18.8337 19.1666 19.7666 18.2337 19.7666 17.0833V0.833252H6.43329V14.9999" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width="20" height="20" fill="white" transform="translate(0.599976)" />
        </clipPath>
      </defs>
    </svg>
  )
}


