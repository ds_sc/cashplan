import React from 'react';
import PropTypes from 'prop-types';

DashboardIcon.propTypes = {
  fill: PropTypes.string.isRequired,
}

export default function DashboardIcon(props) {
  return (
    <svg width="21" height="20" viewBox="0 0 21 20" fill={props.fill} xmlns="http://www.w3.org/2000/svg">
      <path d="M8.34998 1.3125V10.25H3.16248V1.3125H8.34998Z" stroke="#2E2E2E" strokeWidth="2" />
      <path d="M8.34998 14.75V18.6875H3.16248V14.75H8.34998Z" stroke="#2E2E2E" strokeWidth="2" />
      <path d="M18.0375 1.3125V5.25H12.85V1.3125H18.0375Z" stroke="#2E2E2E" strokeWidth="2" />
      <path d="M18.0375 9.75V18.6875H12.85V9.75H18.0375Z" stroke="#2E2E2E" strokeWidth="2" />
    </svg>
  )
}


