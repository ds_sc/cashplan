import React from 'react';
import PropTypes from 'prop-types';

CashflowIcon.propTypes = {
  fill: PropTypes.string.isRequired,
}

export default function CashflowIcon(props) {
  return (
    <svg width="20" height="20" viewBox="0 0 20 20" fill={props.fill} xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
        <path d="M11.6375 11.25H7.96246V19.375H11.6375V11.25Z" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M4.28749 15H0.612488V19.375H4.28749V15Z" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M18.9875 7.5H15.3125V19.375H18.9875V7.5Z" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M3.67499 6.875L6.73749 3.75L9.79999 6.875L15.925 0.625" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" />
        <path d="M12.25 0.625H15.925V4.375" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width="19.6" height="20" fill="white" />
        </clipPath>
      </defs>
    </svg>
  )
}


