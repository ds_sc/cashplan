import React from 'react';
import PropTypes from 'prop-types';

CategoriesIcon.propTypes = {
  fill: PropTypes.string.isRequired,
}

export default function CategoriesIcon(props) {
  return (
    <svg width="21" height="20" viewBox="0 0 21 20" fill={props.fill} xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
        <path d="M3.09998 19.1667V9.16675L10.6 1.66675L18.1 9.16675V19.1667H3.09998Z" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M7.26666 15H13.9333" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
        <path d="M10.6 10.8333C11.5204 10.8333 12.2666 10.0871 12.2666 9.16667C12.2666 8.24619 11.5204 7.5 10.6 7.5C9.67948 7.5 8.93329 8.24619 8.93329 9.16667C8.93329 10.0871 9.67948 10.8333 10.6 10.8333Z" stroke="#2E2E2E" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="square" />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width="20" height="20" fill="white" transform="translate(0.599976)" />
        </clipPath>
      </defs>
    </svg>
  )
}


